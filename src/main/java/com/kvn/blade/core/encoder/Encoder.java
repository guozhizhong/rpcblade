package com.kvn.blade.core.encoder;
/**
* @author wzy
* @date 2017年11月16日 下午6:08:31
*/
public interface Encoder {

	Object encode(Object arg);

}
